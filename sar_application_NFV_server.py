
from collections import Counter
from scapy.all import *
import time 
from datetime import datetime
from threading import Thread, Lock
import numpy as np
import csv
import os.path
from os import path

class MonitoringThread(Thread):
	def __init__(self, iface):
		Thread.__init__(self)
		self.iface = iface
		print 'Ready to receive from ', self.iface


	def custom_action(self, packet):
		if "IP" in packet:
			timestamp_arrival = time.time()
			src = packet["IP"].src
			dst = packet["IP"].dst
			data = packet["Raw"].load
			# data = float(packet["Raw"].load)
			# delay = timestamp_arrival - data
			######################################
			# Write your code here
			######################################
			print src, dst, data


	def run(self):
		conf.L3socket = L3RawSocket
		sniff(filter="icmp", iface=self.iface, prn=self.custom_action)


thread = MonitoringThread("enp0s3")
thread.start()
